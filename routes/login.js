import express from 'express';
const router = express.Router();

//JWT
const jwt = require('jsonwebtoken');

// importar el modelo user
import User from '../models/user';

// Hash Contraseña
const bcrypt = require('bcrypt');
const saltRounds = 10;



router.post('/v1/login', async(req, res)=> {
    const body = req.body;

    try{
      const usuarioDB = await User.findOne({ RFC: body.RFC });

      if (!usuarioDB) {
        return res.status(400).json({
          mensaje: "RFC no encobtrado",
        });
      }

      //Evaluar password
      if (!bcrypt.compareSync(body.pass, usuarioDB.pass)) {
        return res.status(400).json({
          mensaje: "contraseña no encobtrado",
        });
      }

      //generar tokens
      // Generar Token
      let token = jwt.sign(
        {
          data: usuarioDB,
        },
        "secret",
        { expiresIn: 60 * 60 }
      ); // Expira en 30 días

      res.json({
        usuarioDB,
        token,
      });
    }catch(error) {
        return res.status(500).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }
})



module.exports = router;