import mongoose from 'mongoose';

const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;




const userSchema = new Schema({
  nombre: {type: String, required: [true, 'Nombre obligatorio']},
  apellidoPaterno: String,
  apellidoMaterno: String,
  RFC: {type: String, required: [true, 'RFC es obligatorio'], unique:true},
  pass: {type: String, required: [true, 'Pass es obligatorio']},
 
});

// Validator
userSchema.plugin(uniqueValidator, { message: 'Error, esperaba un {PATH} único.' });

//ocultar password
userSchema.methods.toJSON = function() {
    var obj = this.toObject();
    delete obj.pass;
    return obj;
   }

// Convertir a modelo
const User = mongoose.model('User', userSchema);

export default User;