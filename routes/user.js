import express from 'express';
const router = express.Router();

// importar el modelo user
import User from '../models/user';

const {verificarAuth} = require('../middlewares/autenticacion')

// Hash Contraseña
const bcrypt = require('bcrypt');
const saltRounds = 10;

//POST registro
router.post("/v1/user", async(req, res)=> {
    const body = {
        nombre: req.body.nombre,
        apellidoPaterno: req.body.apellidoPaterno,
        apellidoMaterno: req.body.apellidoMaterno,
        RFC: req.body.RFC,
    };

    body.pass = bcrypt.hashSync(req.body.pass, saltRounds);

    try{
        const usarioDB = await User.create(body);
        res.json(usarioDB);
    }catch (error) {
        return res.status(500).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }
});

// POST agregar suario
router.post("/v1/AgregarUser", verificarAuth, async(req, res)=> {
    const body = {
        nombre: req.body.nombre,
        apellidoPaterno: req.body.apellidoPaterno,
        apellidoMaterno: req.body.apellidoMaterno,
        RFC: req.body.RFC,
    };

    body.pass = bcrypt.hashSync(req.body.pass, saltRounds);

    try{
        const usarioDB = await User.create(body);
        res.json(usarioDB);
    }catch (error) {
        return res.status(500).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }
});

// Get con todos los documentos
router.get('/v1/user', verificarAuth, async(req, res) => {
    try {
      const usarioDB = await User.find();
      res.json(usarioDB);
    } catch (error) {
      return res.status(400).json({
        mensaje: 'Ocurrio un error',
        error
      })
    }
  });

//DELETE user
router.delete('/v1/user/:id', verificarAuth, async(req, res) => {
    const _id = req.params.id;
    try {
      const usarioDB = await User.findByIdAndDelete({_id});
      if(!usarioDB){
        return res.status(400).json({
          mensaje: 'No se encontró el id indicado',
          error
        })
      }
      res.json(usarioDB);  
    } catch (error) {
      return res.status(400).json({
        mensaje: 'Ocurrio un error',
        error
      })
    }
  })

module.exports = router;